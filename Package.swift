// swift-tools-version:5.7

import PackageDescription

let package = Package(
    name: "PythonOnMac",
    products: [
        .library(name: "PythonOnMac", targets: ["PythonOnMac"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/nikhilsha/pythononmaccore.git", from: "1.0.0")
    ],
    targets: [
        .target(name: "PythonOnMac", dependencies: [
            "PythonOnMacCore"
        ]),
        .testTarget(name: "PythonOnMacTests", dependencies: ["PythonOnMac"])
    ]
)
